# symfony-webpack-encore-bundle

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/webpack-encore-bundle)
[**symfony/webpack-encore-bundle**](https://packagist.org/packages/symfony/webpack-encore-bundle)

Symfony UX integration with Webpack Encore!

![Packagist Downloads](https://img.shields.io/packagist/dm/symfony/asset-mapper)
[symfony/asset-mapper](https://packagist.org/packages/symfony/asset-mapper)


* https://symfony.com/bundles/WebpackEncoreBundle/current/index.html
* [symfony.com/webpack-encore](https://symfony.com/webpack-encore)
  * [*Managing CSS and JavaScript*
    ](https://symfony.com/doc/current/frontend.html)
* [*The Symfony UX Initiative & Packages*
  ](https://symfony.com/doc/current/frontend/ux.html)
* [ux.symfony.com](https://ux.symfony.com)
* https://github.com/symfony/webpack-encore-bundle
* [*How And Why I Use Symfony Asset Mapper (Importmap) Over Encore*
  ](https://kerrialnewham.com/articles/how-and-why-i-use-symfony-asset-mapper-importmap-over-encore)
  2024-07 Kerrial Newham

# Unofficial documentation
## Comparisons

* [*How And Why I Use Symfony Asset Mapper (Importmap) Over Encore*
  ](https://kerrialnewham.com/articles/how-and-why-i-use-symfony-asset-mapper-importmap-over-encore)
  2024-07 Kerrial Newham

* [*Comparing API Platform and Symfony Encore with ReactJS*
  ](https://medium.com/@julbrs/comparing-api-platform-and-symfony-encore-with-reactjs-e4e91e0fdb6c)
  2019-10 Julien Bras

# npm vs yarn
## Command Translation
* [*npm vs Yarn Command Translation Cheat Sheet*](https://gist.github.com/jonlabelle/c082700c1c249d986faecbd5abf7d65b)
* [*NPM vs Yarn Cheat Sheet*](https://shift.infinite.red/npm-vs-yarn-cheat-sheet-8755b092e5cc)

## Which to use?
* [*Yarn vs npm - which Node package manager to use in 2018?*](https://blog.risingstack.com/yarn-vs-npm-node-js-package-managers/)
* [*Yarn vs npm: Everything You Need to Know*](https://www.sitepoint.com/yarn-vs-npm/)

## Download yarn with npm
* [*Why wouldn't I use npm to install yarn?*](https://stackoverflow.com/questions/40025890/why-wouldnt-i-use-npm-to-install-yarn/40037391)

## Announcement
* [*Yarn: A new package manager for JavaScript*](https://code.fb.com/web/yarn-a-new-package-manager-for-javascript/)

# Specialized tutorial
* [*Symfony 4 with Vue and Semantic*](https://medium.com/@yaroslav429/symfony-4-vue-semantic-ui-b607b9b0bcc8)
